package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.repositories.contracts.BeerRepository;
import com.telerikacademy.beertag.repositories.contracts.TagRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTests {

    @Mock
    TagRepository tagRepository;

    @InjectMocks
    TagServiceImpl service;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAll_Should_ReturnTags() {
        //Arrange
        Mockito.lenient().when(tagRepository.getAll())
                .thenReturn(Arrays.asList(
                        new Tag("Fresh"),
                        new Tag("Cool")
                ));
        //Act
        List<Tag> result = service.getAll();
    }

    @Test
    public void getByName_Should_ReturnTag_When_NameExist() {
        // Arrange
        Mockito.lenient().when(tagRepository.getAll())
                .thenReturn(Arrays.asList(
                        new Tag("Fresh"),
                        new Tag("Cool")
                ));
        Mockito.lenient().when(tagRepository.getByName("Fresh"))
                .thenReturn(new Tag("Fresh"));

        // Act
        List<Tag> result = service.getByName("Fresh");

        //Assert
        Assert.assertEquals("Fresh", result.get(0).getName());

    }


    @Test(expected = IllegalArgumentException.class)
    public void getByName_Should_ThrowException_When_NameNotExist() {
        // Arrange
        Mockito.lenient().when(tagRepository.getAll())
                .thenReturn(Arrays.asList(
                        new Tag("Fresh"),
                        new Tag("Cool")
                ));

        // Act
        Tag result = service.getByName("Hashtag")
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Test
    public void getById_Should_ReturnTag_When_IdExist() {
        // Arrange
        Mockito.lenient().when(tagRepository.getAll())
                .thenReturn(Arrays.asList(
                        new Tag(1, "Fresh"),
                        new Tag(2, "Cool"),
                        new Tag(3, "Hashtag")));

        Mockito.lenient().when(tagRepository.getById(2))
                .thenReturn(new Tag(2, "Cool"));
        // Act
        Tag result = service.getById(2);

        // Assert
        Assert.assertEquals(2, result.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getById_Should_ThrowException_When_TagNotExit() {
        // Arrange
        Mockito.lenient().when(tagRepository.getAll())
                .thenReturn(Arrays.asList(
                        new Tag("Fresh"),
                        new Tag("Cool"),
                        new Tag("Hashtag")));

        // Act
        Tag result = service.getById(10);
    }


    @Test
    public void create_Should_CreateInRepository_When_TagIsValid() {
        //Arrange
        Tag tag = new Tag("Fav");

        //Act
        service.create(tag);

        //Assert
        Mockito.verify(tagRepository, Mockito.times(1)).create(tag);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_NameIsDuplicate() {
        //Arrange
        List<Tag> tags = new ArrayList<>();
        tags.add(new Tag("Hashtag"));
        when(tagRepository.getAll())
                .thenReturn(tags);

        //Act
        service.create(new Tag("Hashtag"));
    }

    @Test
    public void update_Should_UpdateInRepository_When_BeerIsValid() {
        //Arrange
        Tag tag = new Tag(1, "Fresh");
        tagRepository.create(tag);
        tag = new Tag(1, "freshhy");


        //Act
        service.update(1, tag);

        //Assert
        Mockito.verify(tagRepository, Mockito.times(1)).update(1, tag);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_NameIsDuplicate() {

        //Arrange
        List<Tag> tags = new ArrayList<>();
        tags.add(new Tag(1, "Fresh"));
        when(tagRepository.getAll())
                .thenReturn(tags);

        //Act
        service.update(2, new Tag(1, "Fresh"));
    }

    @Test
    public void addBeer_Should_ConnectTagAndBeer() {
        //Arrange
        Tag tag = new Tag(1, "project");
        Mockito.when(tagRepository.getById(1))
                .thenReturn(tag);

        Beer beer = new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "light");

        BeerRepository beerRepository = Mockito.mock(BeerRepository.class);
        Mockito.when(beerRepository.getById(1))
                .thenReturn(beer);

        TagServiceImpl service = new TagServiceImpl(tagRepository, beerRepository);

        //Act
        service.addBeer(1, beer);

        //Assert
        Assert.assertTrue(tag.getBeers().contains(beer));
        Assert.assertTrue(beer.getTags().contains(tag));
    }

    @Test
    public void removeBeer_Should_DisconnectTagAndBeer() {
        //Arrange
        Tag tag = new Tag(1, "project");
        Beer beer = new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "light");
        Mockito.when(tagRepository.getAll())
                .thenReturn(Arrays.asList(
                        new Tag(1, "Fresh"),
                        new Tag(2, "Cool"),
                        new Tag(3, "Hashtag")));

        tag.getBeers().add(beer);
        beer.getTags().add(tag);

        Mockito.when(tagRepository.getById(1))
                .thenReturn(tag);

        BeerRepository beerRepository = Mockito.mock(BeerRepository.class);
        Mockito.when(beerRepository.getById(1))
                .thenReturn(beer);

        TagServiceImpl service = new TagServiceImpl(tagRepository, beerRepository);

        //Act
        service.removeBeer(1, 1);

        //Assert
        Assert.assertFalse(tag.getBeers().contains(beer));
        Assert.assertFalse(beer.getTags().contains(tag));
    }


    @Test
    public void delete_Should_RemoveInRepository() {
        //Arrange
        Tag tag = new Tag(1, "Tagche");

        Mockito.lenient().when(tagRepository.getById(1))
                .thenReturn(tag);
        Mockito.lenient().when(tagRepository.getAll())
                .thenReturn(Arrays.asList(
                        new Tag(1, "Tagche"),
                        new Tag(2, "Fresh"),
                        new Tag(4, "Bubbly")
                ));

        //Act
        service.delete(1);

        //Assert
        Mockito.verify(tagRepository, Mockito.times(1)).delete(1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void delete_Should_ThrowException_When_IdNotExist() {
        //Arrange
        Tag tag = new Tag(1, "BBBBB");

        Mockito.lenient().when(tagRepository.getById(1))
                .thenReturn(tag);

        //Act
        service.delete(2);

        //Assert
        Mockito.verify(tagRepository, Mockito.times(1)).delete(2);
    }
}
