package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.models.User;
import com.telerikacademy.beertag.repositories.contracts.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {
    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserServiceImpl service;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }
//
//    @Test
//    public void get_ShouldReturnList_When_NameIsEmpty() {
//        //Arrange
//        Mockito.when(userRepository.getAll())
//                .thenReturn(Arrays.asList(
//                        new User(145, "Ivan", "Ssimeonov", "ivansssense@gmail.com", "megatv0n", "1234v56"),
//                        new User(2, "Kevin", "Brown", "kevinbrowns@gmail.com", "Marketing", "1999226")));
//
//        //Act
//        List<User> result = service.get(null);
//    }

    @Test
    public void getById_Should_ReturnBeer_When_IdExist() {
        //Arrange
        Mockito.lenient().when(userRepository.getAll())
                .thenReturn(Arrays.asList(
                        new User(145, "Ivan", "Ssimeonov", "ivansssense@gmail.com", "megatv0n", "1234v56"),
                        new User(2, "Kevin", "Brown", "kevinbrowns@gmail.com", "Marketing", "1999226")));


        Mockito.when(userRepository.getById(2))
                .thenReturn(new User(2, "Kevin", "Brown", "kevinbrowns@gmail.com", "Marketing", "1999226"));

        // Act
        User result = service.getById(2);

        // Assert
        Assert.assertEquals(2, result.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getById_Should_ThrowException_When_UserNotExit() {
        // Arrange
        Mockito.lenient().when(userRepository.getAll())
                .thenReturn(Arrays.asList(
                        new User(145, "Ivan", "Ssimeonov", "ivansssense@gmail.com", "megatv0n", "1234v56"),
                        new User(2, "Kevin", "Brown", "kevinbrowns@gmail.com", "Marketing", "1999226")));

        // Act
        User result = service.getById(10);
    }

    @Test
    public void create_Should_CreateInRepository_When_TagIsValid() {
        //Arrange
        User user = new User(145, "Ivan", "Ssimeonov", "ivansssense@gmail.com", "megatv0n", "1234v56");

        //Act
        service.create(user);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).create(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_NameIsDuplicate() {
        //Arrange
        List<User> users = new ArrayList<>();
        users.add( new User(145, "Ivan", "Ssimeonov", "ivansssense@gmail.com", "megatv0n", "1234v56"));
        when(userRepository.getAll())
                .thenReturn(users);

        //Act
        service.create(new User(145, "Ivan", "Ssimeonov", "ivansssense@gmail.com", "megatv0n", "1234v56"));
    }

}
