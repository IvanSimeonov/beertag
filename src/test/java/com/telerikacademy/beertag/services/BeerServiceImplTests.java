package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.repositories.contracts.BeerRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class BeerServiceImplTests {

    @Mock
    BeerRepository beerRepMock;

    @InjectMocks
    BeerServiceImpl service;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void get_Should_ReturnBeer_When_NameExist() {
        // Arrange
        Mockito.when(beerRepMock.getAll())
                .thenReturn(Arrays.asList(
                        new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "light"),
                        new Beer(2, "Heineken", "Nemska Pivovarna Sofia", "Bulgaria", 3.9, "Chisto ot Germaniq", 6, "dark"),
                        new Beer(5, "Zagorka", "Heineken", "Bulgaria", 4, "Chisto ot nqkude", 2, "dark")
                ));

        // Act
        List<Beer> result = service.get(0, "Zagorka");

        //Assert
        Assert.assertEquals("Zagorka", result.get(0).getName());

    }

    @Test(expected = IllegalArgumentException.class)
    public void get_Should_ThrowException_When_NameNotExist() {
        // Arrange
        Mockito.when(beerRepMock.getAll())
                .thenReturn(Arrays.asList(
                        new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "light"),
                        new Beer(4, "Heineken", "Nemska Pivovarna Sofia", "Bulgaria", 3.9, "Chisto ot Germaniq", 6, "dark"),
                        new Beer(5, "Zagorka", "Heineken", "Bulgaria", 4, "Chisto ot nqkude", 2, "dark")
                ));

        // Act
        Beer result = service.get(0, "Vicky")
                .stream()
                .findFirst()
                .orElse(null);
    }


    @Test
    public void getById_Should_ReturnBeer_When_IdExist() {
        // Arrange
        Mockito.when(beerRepMock.getAll())
                .thenReturn(Arrays.asList(
                        new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "light"),
                        new Beer(2, "Heineken", "Nemska Pivovarna Sofia", "Bulgaria", 3.9, "Chisto ot Germaniq", 6, "dark"),
                        new Beer(5, "Zagorka", "Heineken", "Bulgaria", 4, "Chisto ot nqkude", 2, "dark")
                ));

        Mockito.when(beerRepMock.getById(2))
                .thenReturn(new Beer(2, "Heineken", "Nemska Pivovarna Sofia", "Bulgaria", 3.9, "Chisto ot Germaniq", 6, "dark"));

        // Act
        Beer result = service.getById(2);

        // Assert
        Assert.assertEquals(2, result.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getById_Should_ThrowException_When_BeerNotExit() {
        // Arrange
        Mockito.when(beerRepMock.getAll())
                .thenReturn(Arrays.asList(
                        new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "light"),
                        new Beer(4, "Heineken", "Nemska Pivovarna Sofia", "Bulgaria", 3.9, "Chisto ot Germaniq", 6, "dark"),
                        new Beer(5, "Zagorka", "Heineken", "Bulgaria", 4, "Chisto ot nqkude", 2, "dark")
                ));

        // Act
        Beer result = service.getById(10);
    }


    @Test
    public void create_Should_CreateInRepository_When_BeerIsValid() {
        //Arrange
        Beer beer = new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "light");

        //Act
        service.create(beer);

        //Assert
        Mockito.verify(beerRepMock, Mockito.times(1)).create(beer);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_IdIsDuplicate() {
        //Arrange
        List<Beer> beers = new ArrayList<>();
        beers.add(new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "light"));
        when(beerRepMock.getAll())
                .thenReturn(beers);

        //Act
        service.create(new Beer(1, "novabra", "test", "Bulgaria", 3.9, "Chisto ot planinata", 0, "light"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_NameIsDuplicate() {
        //Arrange
        List<Beer> beers = new ArrayList<>();
        beers.add(new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "light"));
        when(beerRepMock.getAll())
                .thenReturn(beers);

        //Act
        service.create(new Beer(3, "Pirinsko", "dew", "Bulgaria", 3.9, "Chistxxo ot planinata", 6, "light"));
    }

    @Test
    public void update_Should_UpdateInRepository_When_BeerIsValid() {
        //Arrange
        Beer beer = new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "light");
        beerRepMock.create(beer);
        beer = new Beer(1, "Pirinnnnsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "light");


        //Act
        service.update(1, beer);

        //Assert
        Mockito.verify(beerRepMock, Mockito.times(1)).update(1, beer);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_NameIsDuplicate() {

        //Arrange
        List<Beer> beers = new ArrayList<>();
        beers.add(new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "light"));
        when(beerRepMock.getAll())
                .thenReturn(beers);

        //Act
        service.update(2, new Beer(2, "Pirinsko", "Pivsovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "light"));
    }


    @Test(expected = IllegalArgumentException.class)
    public void filter_Should_ShowResults_When_StyleExist() {
        // Arrange
        Mockito.when(beerRepMock.getAll())
                .thenReturn(Arrays.asList(
                        new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "dark"),
                        new Beer(4, "Heineken", "Nemska Pivovarna Sofia", "Bulgaria", 3.9, "Chisto ot Germaniq", 6, "dark"),
                        new Beer(5, "Zagorka", "Heineken", "Bulgaria", 4, "Chisto ot nqkude", 2, "dark")
                ));

        //Act
        List<Beer> result = service.filterByStyle("darK");

        // Assert
        Assert.assertEquals(beerRepMock.getAll().stream()
                        .filter(beer -> beer.getStyle().toLowerCase().equals("dark"))
                , result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void filter_Should_ThrowException_When_StyleNotExist() {
        // Arrange
        Mockito.when(beerRepMock.getAll())
                .thenReturn(Arrays.asList(
                        new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "dark"),
                        new Beer(4, "Heineken", "Nemska Pivovarna Sofia", "Bulgaria", 3.9, "Chisto ot Germaniq", 6, "dark"),
                        new Beer(5, "Zagorka", "Heineken", "Bulgaria", 4, "Chisto ot nqkude", 2, "dark")
                ));

        //Act
        List<Beer> result = service.filterByStyle("LiGhT");
    }

    @Test(expected = IllegalArgumentException.class)
    public void filter_Should_ShowResults_When_OriginCountryExist() {
        // Arrange
        Mockito.when(beerRepMock.getAll())
                .thenReturn(Arrays.asList(
                        new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "dark"),
                        new Beer(4, "Heineken", "Nemska Pivovarna Sofia", "Bulgaria", 3.9, "Chisto ot Germaniq", 6, "dark"),
                        new Beer(5, "Zagorka", "Heineken", "Bulgaria", 4, "Chisto ot nqkude", 2, "dark")
                ));

        //Act
        List<Beer> result = service.filterByOriginCountry("Bulgaria");

        // Assert
        Assert.assertEquals(beerRepMock.getAll(), result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void filter_Should_ThrowException_When_OriginCountryNotExist() {
        // Arrange
        Mockito.when(beerRepMock.getAll())
                .thenReturn(Arrays.asList(
                        new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "dark"),
                        new Beer(4, "Heineken", "Nemska Pivovarna Sofia", "Bulgaria", 3.9, "Chisto ot Germaniq", 6, "dark"),
                        new Beer(5, "Zagorka", "Heineken", "Bulgaria", 4, "Chisto ot nqkude", 2, "dark")
                ));

        //Act
        List<Beer> result = service.filterByOriginCountry("Germany");
    }

    @Test
    public void delete_Should_RemoveInRepository() {
        //Arrange
        Beer beer = new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "light");

        when(beerRepMock.getById(1))
                .thenReturn(beer);
        when(beerRepMock.getAll())
                .thenReturn(Arrays.asList(
                        new Beer(2, "Pirinnsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "light"),
                        new Beer(4, "Heineken", "Nemska Pivovarna Sofia", "Bulgaria", 3.9, "Chisto ot Germaniq", 6, "dark"),
                        new Beer(5, "Zagorka", "Heineken", "Bulgaria", 4, "Chisto ot nqkude", 2, "dark")
                ));
        when(beerRepMock.delete(1))
                .thenReturn(beer);

        //Act
        service.delete(1);

        //Assert
        Mockito.verify(beerRepMock, Mockito.times(1)).delete(1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void delete_Should_ThrowException_When_IdNotExist() {
        //Arrange
        Beer beer = new Beer(1, "Pirinsko", "Pivovr", "Bulgaria", 3.9, "Chisto ot planinata", 6, "light");

        when(beerRepMock.getById(1))
                .thenReturn(beer);

        //Act
        service.delete(2);

        //Assert
        Mockito.verify(beerRepMock, Mockito.times(1)).delete(1);
    }

}
