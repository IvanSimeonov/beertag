package com.telerikacademy.beertag.controllers;

import com.telerikacademy.beertag.models.User;
import com.telerikacademy.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class ViewAllUsersController {

    private UserService userService;

    @Autowired
    public ViewAllUsersController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/viewAllUsers")
    public String viewAllUsersPage(Model model){
        List<User> listUsers = userService.getAll();
        model.addAttribute("listUsers",listUsers);
        return "viewAllUsers";
    }

}
