package com.telerikacademy.beertag.controllers;

import com.telerikacademy.beertag.models.User;
import com.telerikacademy.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@Controller
public class CreateAdminController {

    private UserService userService;

    @Autowired
    public CreateAdminController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/createAdmin")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new User());
        return "createAdmin";
    }

    @PostMapping("/createAdmin")
    public String createAdmin(@Valid @ModelAttribute User user, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "All fields must be filled!");
            return "createAdmin";
        }
//        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
//        org.springframework.security.core.userdetails.User newUser =
//                new org.springframework.security.core.userdetails.User(
//                        user.getUsername(), "{noop}" + user.getPassword(), authorities);
//        userDetailsManager.createUser(newUser);
        try {
            userService.createAdmin(user);
        }catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
        return "index";
    }

}
