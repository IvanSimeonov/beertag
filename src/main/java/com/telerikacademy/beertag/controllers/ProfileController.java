package com.telerikacademy.beertag.controllers;

import com.telerikacademy.beertag.models.User;
import com.telerikacademy.beertag.services.UserServiceImpl;
import com.telerikacademy.beertag.services.contracts.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@Controller
public class ProfileController {

    private UserService userService;

    public ProfileController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/profile")
    public String showProfilePage(Model model){
        User user = userService.getByUsername(((UserDetails)(SecurityContextHolder.getContext().getAuthentication().getPrincipal())).getUsername());
        model.addAttribute("user", userService.getByUsername(user.getUsername()));
        return "profile";
    }

    @PostMapping("/profileUpdate")
    public String registerUser(@Valid @ModelAttribute User user, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "All fields must be filled!");
            return "profile";
        }
//        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
//        org.springframework.security.core.userdetails.User newUser =
//                new org.springframework.security.core.userdetails.User(
//                        user.getUsername(), "{noop}" + user.getPassword(), authorities);
//        userDetailsManager.createUser(newUser);
        User user1 = userService.getByUsername(((UserDetails)(SecurityContextHolder.getContext().getAuthentication().getPrincipal())).getUsername());
        User user2 = userService.getById(user1.getId());
        user.setId(user1.getId());
        user.setPassword(user1.getPassword());
        System.out.println(user1.getId());
        System.out.println(user1.getUsername());
        System.out.println(user1.getFirstName());
        System.out.println(user1.getLastName());
        System.out.println(user1.getPassword());

        System.out.println(user2.getId());
        System.out.println(user2.getUsername());
        System.out.println(user2.getFirstName());
        System.out.println(user2.getLastName());
        System.out.println(user2.getPassword());

        System.out.println(user.getId());
        System.out.println(user.getUsername());
        System.out.println(user.getFirstName());
        System.out.println(user.getLastName());
        System.out.println(user.getPassword());
        try {
//            model.addAttribute("user", userService.getByUsername(((UserDetails)(SecurityContextHolder.getContext().getAuthentication().getPrincipal())).getUsername()));

            userService.createTry(user);
        }catch (IllegalArgumentException e){
            System.out.println("error");
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
        return "login";
    }

}
