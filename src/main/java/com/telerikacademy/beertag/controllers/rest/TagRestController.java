package com.telerikacademy.beertag.controllers.rest;


import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.services.contracts.TagService;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tags")
@NoArgsConstructor
public class TagRestController {
    private TagService service;

    @Autowired
    public TagRestController(TagService service) {
        this.service = service;
    }

    @GetMapping
    public List<Tag> get(@RequestParam(required = false) String name) {
        try {
            if (name == null) return service.getAll();
            return service.getByName(name);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Tag getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @GetMapping("/beers/{id}")
    public List<Beer> getTagBeers(@PathVariable int id) {
        return getById(id).getBeers();
    }

    @PostMapping("/new")
    public Tag tag(@Valid @RequestBody Tag tag) {
        try {
            service.create(tag);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return tag;
    }

    @PutMapping("/{id}")
    public Tag update(@PathVariable int id, @RequestBody @Valid Tag tag) {
        try {
            service.update(id, tag);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    e.getMessage()
            );
        }
        return tag;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
