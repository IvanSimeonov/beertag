package com.telerikacademy.beertag.controllers.rest;

import com.telerikacademy.beertag.models.User;
import com.telerikacademy.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private UserService service;

    @Autowired
    public UserRestController(UserService service) {
        this.service = service;
    }

    @GetMapping
    public List<User> getAll() {
        return service.getAll();
    }

    @GetMapping("/username")
    public User getByUsername(@RequestParam String username) {
        return service.getByUsername(username);
    }

    @GetMapping("/email")
    public User getByEmail(@RequestParam String email) {
        return service.getByEmail(email);
    }


    @GetMapping("/{id}")
    public User getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/register")
    public User create(@Valid @RequestBody User user) {
//        try {
        service.create(user);
//        } catch (IllegalArgumentException e) {
//            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
//        }
        return user;
    }

    @PutMapping("/{id}")
    public User updateEmail(@PathVariable int id, @RequestBody String email) {
        try {
            service.updateUserEmail(id, email);
            return getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    e.getMessage()
            );
        }
    }

//    @RequestMapping(value = "/user", method = RequestMethod.GET)
//    public String index(Model model){
//        model.addAttribute("user",service.)
//    }

}
