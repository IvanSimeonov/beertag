package com.telerikacademy.beertag.controllers.rest;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.services.contracts.BeerService;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/beers")
@NoArgsConstructor
public class BeerRestController {
    private BeerService service;

    @Autowired
    public BeerRestController(BeerService service) {
        this.service = service;
    }

    @GetMapping
    public List<Beer> get(@RequestParam(required = false) Integer page, @RequestParam(required = false) String name) {
        try {
            if (page == null) return service.get(1, name);
            return service.get(page, name);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/tags/{id}")
    public List<Tag> getBeerTags(@PathVariable int id) {
        try {
            return service.getBeerTags(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filterS")
    public List<Beer> filterByStyle(@RequestParam String style) {
        try {
            return service.filterByStyle(style);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/filterC")
    public List<Beer> filterByOriginCountry(@RequestParam String country) {
        try {
            return service.filterByOriginCountry(country);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @GetMapping("/sort")
    public List<Beer> sort(@RequestParam(required = false) String param, @RequestParam(required = false) String direction) {
        if (direction == null || direction.isEmpty()) direction = "asc";
        if (param == null || param.isEmpty() || param.toLowerCase().equals("name")) {
            return service.sortByName(direction);
        }
        if (param.toLowerCase().equals("abv")) {
            return service.sortByABV(direction);
        }
        return service.sortByRating(direction);
    }

    @PostMapping("/new")
    public Beer beer(@Valid @RequestBody Beer beer) {
        try {
            service.create(beer);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return beer;
    }

    @PutMapping("/{id}")
    public Beer update(@PathVariable int id, @RequestBody @Valid Beer beer) {
        try {
            service.update(id, beer);
            return beer;
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    e.getMessage()
            );
        }
    }

    @PostMapping("/{id}")
    public void voteRating(@PathVariable int id, @RequestBody double rating) {
        try {
            service.rate(id, rating);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}