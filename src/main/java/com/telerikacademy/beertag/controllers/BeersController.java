package com.telerikacademy.beertag.controllers;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.models.User;
import com.telerikacademy.beertag.services.contracts.BeerService;
import com.telerikacademy.beertag.services.contracts.TagService;
import com.telerikacademy.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@Controller
public class BeersController {

    private BeerService service;
    private UserService userService;
    private TagService tagService;

    @Autowired
    public BeersController(BeerService beerService, UserService userService, TagService tagService) {
        this.service = beerService;
        this.userService = userService;
        this.tagService = tagService;
    }

    @GetMapping("/beers")
    public String showBeersPage(Model model) {
        try {
            model.addAttribute("allBeers", service.get(1, null));
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return "beers";
    }

    @GetMapping("/beersSortName")
    public String showBeerSortName(Model model) {
        try {
            model.addAttribute("allBeers", service.sortByName("asc"));
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return "beersSortName";
    }

    @GetMapping("/beersSortABV")
    public String showBeerSortABV(Model model) {
        try {
            model.addAttribute("allBeers", service.sortByABV("asc"));
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return "beersSortABV";
    }

    @GetMapping("/beersSortRating")
    public String showBeerSortRating(Model model) {
        try {
            model.addAttribute("allBeers", service.sortByRating("asc"));
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return "beersSortRating";
    }


    @GetMapping("/beers/{id}")
    public String showBeerSinglePage(@PathVariable int id, Model model) {
        model.addAttribute("beer", service.getById(id));
        return "beer";
    }

    @GetMapping("/registerbeer")
    public String showRegisterBeerPage(Model model) {
        model.addAttribute("beer", new Beer());
        return "registerbeer";
    }

    @PostMapping("/registerbeer")
    public String registerBeer(@Valid @ModelAttribute Beer beer, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "All fields must be filled!");
            return "registerbeer";
        }
        Beer newBeer = new Beer(0, beer.getName(), beer.getBrewery(), beer.getCountry(),
                beer.getAbv(), beer.getDescription(), beer.getRating(), beer.getStyle());
        try {
            service.create(newBeer);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return "beers";
    }

    @GetMapping("/beers/edit/{id}")
    public String editBeerView(@PathVariable int id, Model model) {
        Beer beer;
        try {
            beer = service.getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        model.addAttribute("beer", beer);
        return "editBeer";
    }

    @RequestMapping(value = "/beers/edit/{id}", method = RequestMethod.POST)
    public String saveBeer(@PathVariable int id, @ModelAttribute("beer") Beer newBeer) {
        Beer beer = new Beer(id, newBeer.getName(), newBeer.getBrewery(),
                newBeer.getCountry(), newBeer.getAbv(), newBeer.getDescription(),
                newBeer.getRating(), newBeer.getStyle());
        try {
            service.update(id, beer);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return "redirect:/beers";
    }

    @RequestMapping(value = "beers/{id}", method = RequestMethod.POST)
    public String rateBeer(@PathVariable int id, String action) {
        try {
            switch (action) {
                case "1":
                    service.rate(id, 5);
                    break;
                case "2":
                    service.rate(id, 4);
                    break;
                case "3":
                    service.rate(id, 3);

                    break;
                case "4":
                    service.rate(id, 2);

                    break;
                case "5":
                    service.rate(id, 1);

                    break;
                default:
                    return "/beers/{id}";
            }
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return "redirect:/beers/{id}";
    }

    @RequestMapping("/beers/delete/{id}")
    public String deleteBeer(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return "redirect:/beers";
    }

    @RequestMapping("/addWishList/{id}")
    public String addBeerWishList(@PathVariable(name = "id") int id) {
        Beer beer = service.getById(id);
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String loggedInUserName = loggedInUser.getName();
        User user = userService.getByUsername(loggedInUserName);
        System.out.println(loggedInUserName);
//        user.getWishList().add(beer);
        userService.createTry(user);
        return "redirect:/beers";
    }

    //    @GetMapping("/beers/addWishList/{id}")
//    public String addBeerWishList(@PathVariable int id, Model model) {
//        Beer beer = service.getById(id);
//        model.addAttribute("beer", beer);
//        return "beers";
//    }
//
//    @RequestMapping(value = "/beers/addWishList/{id}")
//    public String addBeerWishList(@PathVariable int id){
//        Beer beer = service.getById(id);
//        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
//        String loggedInUserName = loggedInUser.getName();
//        System.out.println(loggedInUserName);
//        User user = userService.getByUsername(loggedInUserName);
//        user.getWishList().add(beer);
//        userService.createTry(user);
//        System.out.println(Arrays.toString(user.getWishList().toArray()));
//        return "beers";
//    }

    @GetMapping("/tags")
    public String showTagsPage(Model model) {
        try {
            model.addAttribute("allTags", tagService.getAll());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return "tags";
    }
    @GetMapping("/tags/edit/{id}")
    public String editTagView(@PathVariable int id, Model model) {
        Tag tag;
        try {
            tag = tagService.getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        model.addAttribute("tag", tag);
        return "editTag";
    }

    @RequestMapping(value = "/tags/edit/{id}", method = RequestMethod.POST)
    public String saveTag(@PathVariable int id, @ModelAttribute("tag") Tag newTag) {
        Tag tag=new Tag(id,newTag.getName());
        try {
            tagService.update(id, tag);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return "redirect:/tags";
    }


    @RequestMapping("/tags/delete/{id}")
    public String deleteTag(@PathVariable int id) {
        try {
            tagService.delete(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return "redirect:/tags";
    }

}
