package com.telerikacademy.beertag.repositories;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.repositories.contracts.BeerRepository;
import lombok.NoArgsConstructor;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("BeerRepository")
@NoArgsConstructor
public class BeerRepositoryImpl implements BeerRepository {
    private SessionFactory sessionFactory;

    @Autowired
    BeerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Beer beer) {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Transaction transaction = session.beginTransaction();
        session.save(beer);
        transaction.commit();
    }

    @Override
    public List<Beer> getAll() {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Query<Beer> query = session.createQuery("from Beer", Beer.class);
        return query.list();
    }

    @Override
    public Beer getByName(String name) {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Query<Beer> query = session.createQuery("from Beer where name = :name", Beer.class);
        query.setParameter("name", name);
        return query.list().stream().findFirst().orElse(null);
    }

    @Override
    public Beer getById(int id) {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Beer beer= session.get(Beer.class, id);
        session.close();
        return beer;
    }

    @Override
    public List<Tag> getBeerTags(int id) {
        return getById(id).getTags();
    }

    @Override
    public List<Beer> filterByStyle(String style) {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Query<Beer> query = session.createQuery("from Beer where style = :style", Beer.class);
        query.setParameter("style", style);
        session.close();
        return query.list();
    }

    @Override
    public List<Beer> filterByOriginCountry(String country) {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }

        Query<Beer> query = session.createQuery("from Beer where country =: country", Beer.class);
        query.setParameter("country", country);
        session.close();
        return query.list();
    }

    @Override
    @Modifying
    public Beer update(int id, Beer entity) {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Transaction transaction = session.beginTransaction();
        Beer beer = session.load(Beer.class, id);
        beer.setName(entity.getName());
        beer.setDescription(entity.getDescription());
        session.update(beer);
        transaction.commit();
        session.close();
        return beer;
    }


    @Override
    @Modifying
    public void updateBeerRating(int beerId, double newRating) {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Transaction transaction = session.beginTransaction();
        Beer beer = session.load(Beer.class, beerId);
        beer.setRating(newRating);
        session.update(beer);
        transaction.commit();
        session.close();
    }

    @Override
    @Modifying
    public void updateVotes(int id, int votes) {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Transaction transaction=session.beginTransaction();
        Beer beer = this.getById(id);
        beer.setVotesCount(votes);
        session.update(beer);
        transaction.commit();
        session.close();
    }

    @Override
    public Beer delete(int id) {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Transaction transaction = session.beginTransaction();
        Beer beer = session.load(Beer.class, id);
        session.delete(beer);
        transaction.commit();
        session.close();
        return beer;
    }

}