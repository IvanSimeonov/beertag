package com.telerikacademy.beertag.repositories.contracts;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Tag;

import java.util.List;

public interface BeerRepository {

    void create(Beer entity);

    List<Beer> getAll();

    Beer getByName(String name);

    Beer getById(int id);

    List<Beer> filterByStyle(String style);

    List<Beer> filterByOriginCountry(String originCountry);

    Beer update(int id, Beer entity);

    void updateBeerRating(int beerId, double rating);

    void updateVotes(int id, int votes);

    List<Tag> getBeerTags(int id);

    Beer delete(int id);
}