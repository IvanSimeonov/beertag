package com.telerikacademy.beertag.repositories.contracts;

import com.telerikacademy.beertag.models.Tag;

import java.util.List;

public interface TagRepository {

    void create(Tag entity);

    List<Tag> getAll();

    Tag getByName(String name);

    Tag getById(int id);

    void update(int id, Tag entity);

    void delete(int id);
}