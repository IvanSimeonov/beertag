package com.telerikacademy.beertag.repositories.contracts;

import com.telerikacademy.beertag.models.User;

import java.util.List;

public interface UserRepository {

    void create(User user);

    void createTry(User user);

    List<User> getAll();

    User getById(int id);

    User getByUsername(String name);

    User getByEmail(String email);

    void updateUserEmail(int beerId, String email);

    User delete(User user);

}
