package com.telerikacademy.beertag.repositories;

import com.telerikacademy.beertag.models.Role;
import com.telerikacademy.beertag.repositories.contracts.RoleRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RoleRepositoryImpl implements RoleRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void createTry(Role role) {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();

        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Transaction transaction = session.beginTransaction();
        Role existing = session.load(Role.class, role.getRoleId());
        existing.setUsername(role.getUsername());

        session.update(existing);
        transaction.commit();

    }


}


