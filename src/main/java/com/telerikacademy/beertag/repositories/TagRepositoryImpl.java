package com.telerikacademy.beertag.repositories;

import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.repositories.contracts.TagRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@org.springframework.stereotype.Repository("TagRepository")
public class TagRepositoryImpl implements TagRepository {
    private SessionFactory sessionFactory;

    @Autowired
    TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Tag> getAll() {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Query<Tag> query = session.createQuery("from Tag", Tag.class);
        return query.list();
    }

    @Override
    public Tag getByName(String name) {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Query<Tag> query = session.createQuery("from Tag where name = :name", Tag.class);
        query.setParameter("name", name);
        session.close();
        return query.list()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public Tag getById(int id) {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        return session.get(Tag.class, id);
    }


    @Override
    public void create(Tag entity) {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        session.save(entity);
    }

    @Override
    public void update(int id, Tag entity) {Session session;

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Transaction transaction = session.beginTransaction();
        Tag tag = session.load(Tag.class, id);
        tag.setName(entity.getName());
        session.update(tag);
        transaction.commit();
        session.close();

    }

    @Override
    public void delete(int id) {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Transaction transaction = session.beginTransaction();
        Tag beer = session.load(Tag.class, id);
        session.delete(beer);
        transaction.commit();
        session.close();
    }
}
