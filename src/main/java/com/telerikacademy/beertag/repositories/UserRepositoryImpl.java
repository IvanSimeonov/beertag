package com.telerikacademy.beertag.repositories;

import com.telerikacademy.beertag.models.User;
import com.telerikacademy.beertag.repositories.contracts.UserRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class UserRepositoryImpl implements UserRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User getById(int id) {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        return session.get(User.class, id);
    }

    @Override
    public List<User> getAll() {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Query<User> query = session.createQuery("from User", User.class);
        return query.list();
    }

    @Override
    public User getByUsername(String username) {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);
            return query.list().stream().findFirst().orElse(null);

        } catch (HibernateException e) {
            session = sessionFactory.openSession();
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);
            return query.list().stream().findFirst().orElse(null);
        }
    }

    @Override
    public User getByEmail(String email) {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);
            return query.list().stream().findFirst().orElse(null);

        } catch (HibernateException e) {
            session = sessionFactory.openSession();
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);
            return query.list().stream().findFirst().orElse(null);

        }
    }

    @Override
    public void create(User user) {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
            session.save(user);
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
            session.save(user);
        }
    }

    @Override
    public void createTry(User user) {
        Session session;

        try {
            session = sessionFactory.getCurrentSession();

        } catch (HibernateException e) {
            session = sessionFactory.openSession();
        }
        Transaction transaction = session.beginTransaction();
        User existing = session.load(User.class, user.getId());

        existing.setUsername(user.getUsername());
        existing.setFirstName(user.getFirstName());
        existing.setLastName(user.getLastName());
        existing.setEmail(user.getEmail());
        existing.getRole().setUsername(user.getUsername());
        existing.setPassword(user.getPassword());
//        existing.setWishList(user.getWishList());
        session.update(existing);
        transaction.commit();

    }

    @Override
    @Modifying
    public void updateUserEmail(int userId, String email) {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
            Transaction transaction = session.beginTransaction();
            User user = session.load(User.class, userId);
            user.setEmail(email);
            session.update(user);
            transaction.commit();
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            User user = session.load(User.class, userId);
            user.setEmail(email);
            session.update(user);
            transaction.commit();
        }
    }

    @Override
    public User delete(User user) {
        Session session;
        try {
            session = sessionFactory.getCurrentSession();
            Transaction transaction = session.beginTransaction();
            session.delete(user.getRole());
            session.delete(user);
            transaction.commit();
            return user;
        } catch (HibernateException e) {
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            session.delete(user.getRole());
            session.delete(user);
            transaction.commit();
            return user;

        }
    }
}
