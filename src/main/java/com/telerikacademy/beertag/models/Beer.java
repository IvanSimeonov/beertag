package com.telerikacademy.beertag.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

//filtered by tags, style, origin country
@Entity
@Table(name = "beers")
@Getter
@Setter
@NoArgsConstructor
public class Beer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    @Size(min = 3, max = 200, message = "Beer name must be with length in 3 to 200 symbols")
    private String name;

    @Column(name = "brewery")
    @Size(min = 1, max = 200, message = "Brewery name must be with length in 1 to 200 symbols")
    private String brewery;

    @Column(name = "country")
    @Size(min = 2, max = 150, message = "Origin country name must be with length in 2 to 150 symbols")
    private String country;

    @Column(name = "abv")
    private double abv;

    @Column(name = "description")
    private String description;

    @Column(name = "rating", columnDefinition = "0")
    @Min(value = 0, message = "You cannot give negative rating")
    private double rating;

    @Column(name = "style")
    private String style;

    //    @ManyToMany(mappedBy = "beer_tags" )
    @JsonIgnore
    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "beer_tags",
            joinColumns = {@JoinColumn(name = "beer_id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_id")}
    )
    private List<Tag> tags = new ArrayList<>();

    @Column(name = "votes_count", columnDefinition = "0")
    private int votesCount = 0;


    public static final int PAGE_MAX = 9;

    //Parameters
    public Beer(int id, String name, String brewery, String country, double abv, String description, double rating, String style) {
        this.name = name;
        this.brewery = brewery;
        this.country = country;
        this.abv = abv;
        this.description = description;
        this.rating = rating;
        this.style = style;
    }
}