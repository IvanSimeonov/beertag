package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.repositories.contracts.BeerRepository;
import com.telerikacademy.beertag.repositories.contracts.TagRepository;
import com.telerikacademy.beertag.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    private TagRepository tagRepository;
    private BeerRepository beerRepository;

    @Autowired
    public TagServiceImpl(@Qualifier("TagRepository") TagRepository tagRepository,@Qualifier("BeerRepository") BeerRepository beerRepository) {
        this.tagRepository = tagRepository;
        this.beerRepository = beerRepository;
    }

    @Override
    public void create(Tag tag) {
        checkExistingName(tag);
        tagRepository.create(tag);
    }


    @Override
    public List<Tag> getAll() {

        //       if (tags == null || tags.isEmpty()) throw new IllegalArgumentException("There are no tags");
        return tagRepository.getAll();
    }

    @Override
    public List<Tag> getByName(String name) {
        List<Tag> tags=new ArrayList<>();
        Tag tag = tagRepository.getByName(name);
        if (tag==null) throw new IllegalArgumentException(String.format("No tag with name %s", name));
        tags.add(tag);
        return tags;
    }

    @Override
    public Tag getById(int id) {
        Tag tag = tagRepository.getById(id);
        if (tag == null) {
            throw new IllegalArgumentException(
                    String.format("Tag with id %d not found.", id));
        }
        return tag;
    }

    @Override
    public void update(int id, Tag tag) {
        checkExistingName(tag);

        tagRepository.update(id, tag);
    }

    @Override
    public void addBeer(int tagId, Beer beer) {
        Tag tag = tagRepository.getById(tagId);
        if (tag.getBeers().contains(beer)) {
            throw new IllegalArgumentException(String.format("Beer already has %s tag",
                    tag.getName()));
        }
        Beer relatedBeer = beerRepository.getById(beer.getId());

        tag.getBeers().add(beer);
        relatedBeer.getTags().add(tag);
    }

    @Override
    public void removeBeer(int tagId, int beerId) {
        Tag tag = tagRepository.getById(tagId);
        Beer beer = beerRepository.getById(beerId);
        if (tag == null)
            throw new IllegalArgumentException(String.format("Tag with id %s doesn't exists", tagId));
        if (beer == null)
            throw new IllegalArgumentException(String.format("Beer with id %s doesn't exists", beerId));
        tagRepository.getAll().remove(tag);
        beerRepository.getById(beerId).getTags().remove(tag);

    }

    @Override
    public void delete(int id) {
        Tag toRemove = tagRepository.getById(id);
        if (toRemove == null)
            throw new IllegalArgumentException("You cannot delete not existing tag.");
        tagRepository.delete(id);
    }

    private void checkExistingName(Tag tag) {
        Tag existing = tagRepository.getAll().stream()
                .filter(x -> x.getName().toLowerCase().equals(tag.getName().toLowerCase()))
                .findFirst()
                .orElse(null);
        if (existing != null)
            throw new IllegalArgumentException(String.format("Tag with name %s already exists.", tag.getName()));
    }
}
