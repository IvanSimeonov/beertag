package com.telerikacademy.beertag.services;

import com.telerikacademy.beertag.models.Role;
import com.telerikacademy.beertag.models.User;
import com.telerikacademy.beertag.repositories.contracts.RoleRepository;
import com.telerikacademy.beertag.repositories.contracts.UserRepository;
import com.telerikacademy.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository){
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getByUsername(String username) {
        User user = userRepository.getByUsername(username);
        if (user == null) {
            throw new IllegalArgumentException(String.format("User with name %s doesn't exists",username));
        }
        return userRepository.getByUsername(username);
    }

    @Override
    public User getByEmail(String email){
        User user = userRepository.getByEmail(email);
        if (user == null){
            throw new IllegalArgumentException(String.format("User with email %s doesn't exists",email));
        }
        return userRepository.getByEmail(email);
    }

    @Override
    public User getById(int id) {
        User user = userRepository.getById(id);
        if (user == null){
            throw new IllegalArgumentException(String.format("User with id %d doesn't exist.",id));
        }
        return user;
    }

    @Override
    public void create(User user) {
        createBoost(user, "ROLE_USER");
    }

    @Override
    public void createAdmin(User user) {
        createBoost(user, "ROLE_ADMIN");
    }

    private void createBoost(User user, String role_admin) {
        List<User> users = userRepository.getAll().stream()
                .filter(user1 -> user1.getUsername().equals(user.getUsername()))
                .collect(Collectors.toList());

        if (users.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("User with name %s already exists", user.getUsername()));
        }
        Role role = new Role(role_admin, user.getUsername());
        user.setPassword("{noop}"+user.getPassword());
        user.setRole(role);
        userRepository.create(user);
    }


    @Override
    public void createTry(User user) {
        userRepository.createTry(user);
    }

    @Override
    public void updateUserEmail(int id, String email) {
        User user = this.getById(id);
        userRepository.updateUserEmail(id, email);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

}
