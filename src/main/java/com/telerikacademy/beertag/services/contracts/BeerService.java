package com.telerikacademy.beertag.services.contracts;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Tag;

import java.util.List;

public interface BeerService {
    void create(Beer beer);

    List<Beer> get(int page, String name);

    Beer getById(int id);

    List<Tag> getBeerTags(int id);

    List<Beer> filterByStyle(String style);

    List<Beer> filterByOriginCountry(String country);

    List<Beer> sortByName(String direction);

    List<Beer> sortByABV(String direction);

    List<Beer> sortByRating(String direction);


    void update(int id, Beer beer);

    void rate(int id, double rating);

    void delete(int id);
}
