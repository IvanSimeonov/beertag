package com.telerikacademy.beertag.services.contracts;

import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Tag;

import java.util.List;

public interface TagService {
    void create(Tag tag);

    List<Tag> getAll();

    List<Tag> getByName(String name);

    Tag getById(int id);

    void update(int id, Tag tag);

    void addBeer(int tagId, Beer beer);

    void removeBeer(int tagId, int beerId);

    void delete(int id);

}
