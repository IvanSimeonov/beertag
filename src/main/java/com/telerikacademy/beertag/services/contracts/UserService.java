package com.telerikacademy.beertag.services.contracts;

import com.telerikacademy.beertag.models.User;

import java.util.List;

public interface UserService {

    User getById(int id);

    void create(User user);

    void createAdmin(User user);

    List<User> getAll();

    User getByUsername(String username);

    User getByEmail(String email);

    void updateUserEmail(int id, String email);

    void delete(User user);

    void createTry(User user);
}
