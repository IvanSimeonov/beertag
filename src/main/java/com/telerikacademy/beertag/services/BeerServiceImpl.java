package com.telerikacademy.beertag.services;


import com.telerikacademy.beertag.models.Beer;
import com.telerikacademy.beertag.models.Tag;
import com.telerikacademy.beertag.repositories.contracts.BeerRepository;
import com.telerikacademy.beertag.repositories.contracts.TagRepository;
import com.telerikacademy.beertag.services.contracts.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.telerikacademy.beertag.models.Beer.PAGE_MAX;

@Service
public class BeerServiceImpl implements BeerService {
    private static final String BEER_WITH_ID_NOT_FOUND = "Beer with id %d not found.";
    private BeerRepository repository;
    private TagRepository tagRepository;

    @Autowired
    public BeerServiceImpl(@Qualifier("BeerRepository") BeerRepository repository, @Qualifier("TagRepository") TagRepository tagRepository) {
        this.repository = repository;
        this.tagRepository = tagRepository;
    }

    @Override
    public List<Beer> get(int page, String name) {
        List<Beer> beers = new ArrayList<>();

        int toIndex = page * PAGE_MAX;
        int repSize = repository.getAll().size();
        if (toIndex > repSize) toIndex = repSize;
        int fromIndex = (page - 1) * PAGE_MAX;
        if (fromIndex > toIndex) throw new IllegalArgumentException("Invalid page");
        if (fromIndex < 0) fromIndex = 0;

        if (name == null || name.isEmpty()) {
            return repository.getAll().subList(fromIndex, toIndex);
        }
        Beer beer = repository.getAll().stream()
                .filter(beer1 -> beer1.getName().equals(name))
                .findFirst()
                .orElse(null);
        if (beer == null) throw new IllegalArgumentException(String.format("No beer with name %s", name));
        beers.add(beer);
        return beers;
    }

    @Override
    public Beer getById(int id) {
        Beer beer = repository.getById(id);
        if (beer == null) {
            throw new IllegalArgumentException(
                    String.format(BEER_WITH_ID_NOT_FOUND, id));
        }
        return beer;
    }

    @Override
    public void create(Beer beer) {
        checkBeerName(beer);
        checkBeerId(beer);
        beer.getTags().forEach(tag -> existingTag(tag));
        repository.create(beer);
    }


    @Override
    public void rate(int id, double rating) {
        Beer beer = this.getById(id);
        if (rating < 0) throw new IllegalArgumentException("Wrong rating");
        double oldRating = beer.getRating() * beer.getVotesCount() + rating;
        int newVotes = beer.getVotesCount() + 1;
        repository.updateVotes(id, newVotes);
        repository.updateBeerRating(id, oldRating / newVotes);
    }

    @Override
    public void update(int id, Beer beer) {
        Beer checkBeer = repository.getAll().stream()
                .filter(beer1 -> beer1.getId() == id)
                .findFirst().orElse(null);
        if (checkBeer == null) {
            throw new IllegalArgumentException(String.format(BEER_WITH_ID_NOT_FOUND, id));
        }
        repository.update(id, beer);
    }


    @Override
    public List<Beer> filterByStyle(String style) {
        List<Beer> beers = repository.filterByStyle(style);
        if (beers == null || beers.isEmpty())
            throw new IllegalArgumentException(String.format("There are no beers with %s style", style));
        return beers;
    }

    @Override
    public List<Beer> filterByOriginCountry(String country) {
        List<Beer> beers = repository.filterByOriginCountry(country);
        if (beers == null || beers.isEmpty())
            throw new IllegalArgumentException(String.format("There are no beers from %s ", country));
        return beers;
    }

    @Override
    public List<Beer> sortByName(String direction) {
        List<Beer> beers = repository.getAll();
        if (direction.toLowerCase().equals("desc")) {
            beers.sort(Comparator.comparing(Beer::getName).reversed());
        } else beers.sort(Comparator.comparing(Beer::getName));
        return beers;
    }

    @Override
    public List<Beer> sortByABV(String direction) {
        List<Beer> beers = repository.getAll();
        if (direction.toLowerCase().equals("desc")) {
            beers.sort(Comparator.comparing(Beer::getAbv).reversed());
        } else beers.sort(Comparator.comparing(Beer::getAbv));
        return beers;
    }

    @Override
    public List<Beer> sortByRating(String direction) {
        List<Beer> beerList = repository.getAll();
        if (direction.toLowerCase().equals("desc")) {
            beerList.sort(Comparator.comparing(Beer::getRating).reversed());
        } else beerList.sort(Comparator.comparing(Beer::getRating));

        return beerList;
    }

    @Override
    public List<Tag> getBeerTags(int id) {
        return repository.getBeerTags(id);
    }

    @Override
    public void delete(int id) {
        List<Tag> tags = getBeerTags(id);
        Beer beer = getById(id);
        if (tags != null && !tags.isEmpty()) {
            tags.forEach(tag -> tag.getBeers().remove(beer));
        }
        repository.delete(id);
    }

    private void checkBeerName(Beer beer) {
        List<Beer> list = repository.getAll();
        if (list == null || list.isEmpty()) return;

        List<Beer> beers = list.stream()
                .filter(beer1 -> beer1.getName().toLowerCase()
                        .equals(beer.getName().toLowerCase()))
                .collect(Collectors.toList());
        if (beers.size() > 0) {
            throw new IllegalArgumentException(
                    String.format("Beer with name %s already exist", beer.getName()));
        }

    }

    private void checkBeerId(Beer beer) {
        List<Beer> beersWithSameId = repository.getAll().stream()
                .filter(beer1 -> beer1.getId() == beer.getId())
                .collect(Collectors.toList());

        if (beersWithSameId.size() > 0)
            throw new IllegalArgumentException(String.format("Beer with id %d already exists.", beer.getId()));
    }

    private void existingTag(Tag tag) {
        if (!tagRepository.getAll().contains(tag)) {
            tagRepository.create(tag);
        }
    }
}

// All beers are listed and can be sorted by rating, ABV, alphabetically